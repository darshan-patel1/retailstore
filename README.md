# Online retail store

![High Level Diagram](https://gitlab.com/darshan-patel1/retailstore/-/raw/main/HighLevelDiagram.jpeg)

# Technologies
- Java 1.8
- Spring Boot 2.6.4
- JPA with Hibernate
- Apache Kafka
- H2 database(In memory)
- Spring Cloud Gateway

# Compile and Test
- Goto root directory(retailstore) and run following command.

`$ ./mvn install`

It will generate executable jars.

- Start Apache Kafka on 9092 port
- Goto retail-gateway(8080) and configure application.properties and run jar
- Goto retail-user(9090) and configure application.properties and run jar
- Goto retail-order(8081) and configure application.properties and run jar
- Goto retail-inventory(8082) and configure application.properties and run jar
- Goto retail-payment(8083) and configure application.properties and run jar


# Default Data

**User**

| User Name | Password |
| ------ | ------ |
| test1 | Test@123 |
| test2 | Test@123 |

**Inventory**

| Product Id | Stock |
| ------ | ------ |
| 1 | 5000 |
| 2 | 3000 |
| 3 | 4200 |
| 4 | 20000 |
| 5 | 9000 |

**User Balance**

| Product Id | Stock |
| ------ | ------ |
| 1 | 50000 |
| 2 | 30000 |
| 3 | 42000 |
| 4 | 20000 |
| 5 | 90000 |

# Postman collections

[Postman collections](https://www.getpostman.com/collections/1d6b7fe3fcfd8a20361c)
