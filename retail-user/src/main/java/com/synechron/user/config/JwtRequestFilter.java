package com.synechron.user.config;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.synechron.user.exception.InvalidTokenStructureException;
import com.synechron.user.service.JwtUserDetailsService;

import io.jsonwebtoken.ExpiredJwtException;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class JwtRequestFilter extends OncePerRequestFilter {

	@Autowired
	private JwtUserDetailsService jwtUserDetailsService;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws ServletException, IOException {

		String authHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
		if(authHeader!=null) {
			String[] parts = authHeader.split(" ");
			
			if(parts.length !=2 || !"Bearer".equals(parts[0])) {
				throw new InvalidTokenStructureException("Incorrect Auth Structure");
			}
			
			final String requestTokenHeader = parts[1] ;
	
			String username = null;
			String jwtToken = null;
			requestTokenHeader.split(" ");
			
			if (requestTokenHeader != null ) {
				jwtToken = requestTokenHeader.substring(0);
				try {
					username = jwtTokenUtil.getUsernameFromToken(jwtToken);
				} catch (IllegalArgumentException e) {
					log.info("Unable to get JWT Token");
				} catch (ExpiredJwtException e) {
					log.info("JWT Token has expired");
				}
			} 
	
	
			if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
	
				UserDetails userDetails = this.jwtUserDetailsService.loadUserByUsername(username);
	
					if (jwtTokenUtil.validateToken(jwtToken, userDetails)) {
	
					UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
							userDetails, null, userDetails.getAuthorities());
					usernamePasswordAuthenticationToken
							.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
					SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
				}
			}
		}
		chain.doFilter(request, response);
	}

}
