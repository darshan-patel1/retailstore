package com.synechron.user.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.synechron.user.entity.User;
import com.synechron.user.repository.UserRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class JwtUserDetailsService implements UserDetailsService {
	@Autowired
	private UserRepository userRepository;

	private PasswordEncoder bcryptEncoder = new BCryptPasswordEncoder();

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findByUsername(username);
		if (user == null) {
			throw new UsernameNotFoundException("User not found with username: " + username);
		}
		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
				new ArrayList<>());
	}

	public User save(User user) {
		User newUser = new User();
		newUser.setUsername(user.getUsername());
		newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
		return userRepository.save(newUser);
	}
	
	@PostConstruct
	public void initInventoryInDB() {
		log.info("Adding default users");
		userRepository.saveAll(
				Stream.of(
						new User("test1", "$2a$10$LCii8qS1cOkyRP0/Z62daeArAaDTYFA9eA7hGaGvnDS.nFp5/vKJS"),
						new User("test2", "$2a$10$LCii8qS1cOkyRP0/Z62daeArAaDTYFA9eA7hGaGvnDS.nFp5/vKJS"))
				.collect(Collectors.toList()));
	}

	public List<User> getAllUsers() {
		return userRepository.findAll();
	}
	
}